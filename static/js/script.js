$(function () {
    $('.up').on('click', function () {
        var item = $(this).closest('.card');
        item.insertBefore(item.prev());
    });
    $('.down').on('click', function () {
        var item = $(this).closest('.card');
        item.insertAfter(item.next());
    });
});

$(".text").on('click', function(e){
    var panel = $(this).parent().siblings('.card-body');
    if (e.target.className != "down" && e.target.className != "up"){
        panel.toggle();
    }
})
